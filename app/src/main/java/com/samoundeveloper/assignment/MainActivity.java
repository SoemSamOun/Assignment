package com.samoundeveloper.assignment;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    TextView tvDate;
    TextView tvDescription;
    TextView tvTile;
    TextView tvCategery;
    TextView tvAthor;
    ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvAthor= (TextView) findViewById(R.id.tvAuthor);
        tvCategery= (TextView) findViewById(R.id.tvCategery);
        tvDate= (TextView) findViewById(R.id.tvDate);
        tvDescription= (TextView) findViewById(R.id.tvDescription);
        tvTile= (TextView) findViewById(R.id.tvTitle);
        imageView= (ImageView) findViewById(R.id.imageView);
    }
}
